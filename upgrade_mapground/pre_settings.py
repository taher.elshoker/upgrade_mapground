import os
from distutils.sysconfig import get_python_lib

if os.name == 'nt':
    os.environ['Path'] = (r'%s\osgeo;' % (get_python_lib())) + os.environ['Path']

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

os.environ.setdefault('APPS_DIR', os.path.abspath(os.path.join(BASE_DIR, "apps")))

os.environ.setdefault('DEFAULT_BACKEND_UPLOADER', 'geonode.importer')

# os.environ['Path'] = r'C:\Users\naybe\Envs\mapground_venv\Lib\site-packages\osgeo;' + os.environ['Path']
os.environ['GEOS_LIBRARY_PATH'] = r'C:\Users\naybe\Envs\mapground_venv\Lib\site-packages\osgeo\geos_c.dll'
os.environ['GDAL_LIBRARY_PATH'] = r'C:\\Users\\naybe\\Envs\\mapground_venv\\Lib\\site-packages\\osgeo\\gdal301.dll'
